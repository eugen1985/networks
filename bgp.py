#! /usr/bin/python3
import sys
import telnetlib
import re
import struct
import netaddr
from netaddr import IPNetwork
import mysql.connector

host = "route-views.routeviews.org"
user = "rviews"

mydb = mysql.connector.connect(
  host="localhost",
  user="net",
  passwd="134679",
  database="networks"
)

netlist = list()
insertlist = list()
mycursor = mydb.cursor()

def mInsert(val):
    sql = "INSERT IGNORE INTO networks (net, provider) VALUES (%s, %s)"
    mycursor.executemany(sql, val)
    mydb.commit()
print('Script start')
tn = telnetlib.Telnet(host)
tn.write(user.encode('ascii') + b"\n")
tn.write("term length 0".encode('ascii') + b"\n")
tn.write("show ip bgp regexp _197695$".encode('ascii') + b"\n")  
sess_op = str(tn.read_all())
items = re.findall(r'[0-9]{1,3}[\.][0-9]{1,3}[\.][0-9]{1,3}[\.][0-9]{1,3}\/[0-9]{1,2}',sess_op)
for i in items:
    netlist.append(IPNetwork(i))
for res in netaddr.cidr_merge(netlist):
    insertlist.append((str(res.cidr),'197695'))
mInsert(insertlist)
print('Script complite')